## Aseyran | Curses
### Introduction
Out of curiosity, I decided to embark on the journey of writing a console based game,
using ruby and the curses library

### What is this?
It is a console based game, being written in ruby.

### Dependencies
colorize
curses

### Features currently
```
Viewports
Elements
String manipulation tools (might need to rewrite them in C)
```

### Future Plans
A few things that would be really nice to add later:
```
Some form of audio playback, or even midi!
```

### Whats next?
Some Game Logic
