#
# format_helper.rb
#
module FormatHelper

  def self.test_fout
    puts ">>$ Running FOut tests"
    puts FOut.bang("Bang")
    puts FOut.bangbang("Bang Bang")
    puts FOut.error("Error")
    puts FOut.question("Question")
    puts FOut.result("Result")
    puts FOut.warning("Warning")
  end

  module FOut end
  class << FOut
    
    def bang(str)
      ">>! ".colorize(:light_yellow) + str
    end

    def bangbang(str)
      ">!! ".colorize(:yellow) + str
    end

    def error(str)
      "!!! ".colorize(:red) + str
    end

    def question(str)
      ">>? ".colorize(:light_blue) + str
    end

    def prompt(str)
      ">># ".colorize(:white) + str
    end

    def result(str)
      ">>$ ".colorize(:light_green) + str
    end

    def warning(str)
      ">$! ".colorize(:light_red) + str
    end

  end  

end

include FormatHelper
