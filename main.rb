#
# main.rb
#

# Encoding Magic!
Encoding.default_internal = Encoding.default_external = "UTF-8"

# Require
require 'curses'
include Curses

begin
  require 'colorize'
rescue(LoadError)  

  puts ">!! colorize has not been loaded, falling back on empty style"
  class String

    def colorize!
      self
    end

    def colorize
      self.dup
    end

  end

end

##
# require_local
# 
src = (Dir["src/*.rb"] + Dir["src/**/*.rb"])
#std_ext = (Dir["std_ext/*.rb"] + Dir["std_ext/**/*.rb"])

require_relative "format_helper.rb"

for f in src.uniq.sort
  loaded = false
  begin
    require_relative f
    loaded = true
  rescue Exception => ex
    p ex
    loaded = false
  ensure
    if loaded 
      puts FOut.result("Loaded: #{f}")
    else
      puts FOut.error("Failed: #{f}")
      exit
    end  
  end  
  
end  

#FormatHelper.test_fout

##
# main(String[] argsv)
#
def main(argsv)
  #array = ["#" * 8] * 8 
  #puts array
  #array = StringTool.crop(array.join("\n"), Rect.new(0, 0, 4, 4))
  #puts array

  Linara.load

  #Curse_OS.boot

  init_screen # // Initialize curse screen
  crmode

  $gague = ECurse_Gauge.new(80 - 4, 1)
  $gague.x = 1
  $gague.y = 22 - 2

  Main.run {
    $gague.rate += 1 / 360.0
    $gague.rate %= 1.0
  }
end

begin
  main(ARGV.dup)
rescue(Exception) => ex
  raise ex  
ensure 
  Linara.unload # Run unloading blocks
  close_screen # Close curses screen
end

