#encoding:UTF-8
#
# src/core/daemon.rb
#

class Daemon

  def initialize
    Daemon.add_daemon(self)
  end

  def init
  end
  
  def dispose
    Daemom.rem_daemon(self)
  end

  ## 
  # update
  #
  def update
    
  end

end

class << Daemon

  def init
    @daemons = []
  end

  def init_daemons
    @daemons.each do |d|
      d.init
      puts FOut.result("Initialized: #{d.class.name}")
    end
  end

  def update
    @daemons.each(&:update)
  end

  def add_daemon(daemon)
    @daemons.push(daemon) unless @daemons.include?(daemon)
  end

  def rem_daemon(daemon)
    @daemons.delete(daemon)
  end

  def clear_daemons()
    @daemons.clear
  end

end
