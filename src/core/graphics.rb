#encoding:UTF-8
#
# src/core/graphics.rb
#
module Graphics

  class << self

    attr_accessor :frames

    def add_drawable(obj)
      raise("#{obj.class} must implement the IDrawable interface (module)") unless obj.kind_of?(IDrawable)
      unless @drawable.include?(obj)
        @drawable.push(obj)
        puts FOut.bang("Added a new drawable object: #{obj}")
      end  
    end

    def rem_drawable(obj)
      @drawable.delete(obj)
    end

    def draw_obj(obj)
      obj.draw if obj.can_draw?
    end

  end

  def self.addstr_at(x, y, str, target=@main_window)
    str.split("\n").each_with_index do |s, i|
      target.setpos(y + i, x)
      target.addstr(s)
    end  
  end

  def self.addstr_at_content(x, y, str, target=@main_window)
    nx, ny, nz, nstr = @main_viewport.translate(x, y, 0, str)
    addstr_at(nx, ny, str, target)
  end

  def self.draw!
    @main_window.clear
    @main_window.setpos(0, 0)
    # █ ─ 
    #@main_window.box("|", "-")
    #@main_window.box("@", "@")
    #@main_window.attrset()

    @drawable.each(&method(:draw_obj))

    @main_window.setpos(0, 0)
    #(1000000).times do 
    #  1 + 1 + 2 ** 8
    #end
  end
  
  def self.init
    reset_frames
    @drawable = []
    #@frame = Curse_Frame.new
    @time_before = 0.0
    @fps = 0.0

    x      = 0
    y      = 0
    width  = 80  
    height = 24

    @main_window = Window.new(height, width, y, x)

    x += 1
    y += 1
    width -= 2
    height -= 2

    @main_viewport = Curse_Viewport.new(x, y, width, height)
  end

  def self.frame_rate
    60.0
  end

  def self.width
    80
  end

  def self.height
    24
  end

  def self.fps
    @fps
  end

  def self.reset_frames
    @frames = 0
  end

  def self.update
    draw!
    @main_window.refresh
    sleep 1 / frame_rate
    n = Time.now.to_f
    @fps = (1 / (n - @time_before)) if @frames % frame_rate == 0 #* frame_rate #(1 - (n - @time_before)) * frame_rate
    @time_before = n
    @frames += 1
  end

end
