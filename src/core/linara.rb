# 
# src/core/linara.rb
# 
module Linara
  
  @load_list = []
  @unload_list = []
  @@loaded = false
  @@unloaded = false
  @log = nil

  class << self ; attr_accessor :log ; end

  ##
  # Linara.add_load(String id, int priority, &func)
  #
  def self.add_load(id, priority=0, &func)
    @load_list.push([priority, id, func])
    self
  end

  def self.add_unload(id, priority=0, &func)
    @unload_list.push([priority, id, func])
    self
  end

  def self.load
    puts FOut.bang("Linara now loading")
    list = @load_list.sort_by(&:first)
    list.each do |(priority, id, func)|
      begin
        func.call
        puts FOut.result("Loaded #{id} with priority #{priority}\n")
      rescue Exception => ex
        puts FOut.error(ex.inspect)
      end  
    end
    @@loaded = true
    self
  end

  def self.unload
    puts FOut.bang("Linara now unloading")
    list = @unload_list.sort_by(&:first)
    list.each do |(priority, id, func)|
      begin
        func.call
        puts FOut.result("Unloaded #{id} with priority #{priority}\n")
      rescue Exception => ex
        puts FOut.error(ex.inspect)
      end  
    end
    @@unloaded = true
    self
  end

end
