#
# src/core/main.rb
#
module Main
  
  def self.init
  end

  def self.run
    loop do
      yield
      Daemon.update
      Graphics.update
    end
  end

end
