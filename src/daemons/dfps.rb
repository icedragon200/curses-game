#
# src/daemons/dfps.rb
#
class DFPS < Daemon

  class FPS_Frame < Curse_Frame

    SPF_FPS = "FPS: %02d"

    def initialize
      super()
      @x, @y = 4, 22
    end

    def content
      SPF_FPS % Graphics.fps.to_i 
    end

  end

  def init
    super()
    @frame = FPS_Frame.new
  end

  def update
    super
    #Graphics.addstr_at(40, 23, Graphics.frames.to_s)
  end

end
