#
# src/daemons/dtasks.rb
#
class DTask < Daemon

  class Task

    def initialize(frames, &func)
      @timer = frames
      @terminated = false
      @executed = false
      @func = func
    end

    def tupdate
      comp = false
      update
      comp = completed?
      return false if comp or terminated?
    end

    def update
      @timer = MathTool.max(@timer - 1, 0) if @timer > 0
      exec if @timer == 0
    end

    def exec
      @func.call
      @executed = true
    end

    def completed?
      @executed and @timer == 0
    end

    def terminated?
      !!@terminated
    end

  end

  def init
    super
    @tasks = []
  end

  def update
    super
    @tasks.reject!(&:tupdate)
  end

end
