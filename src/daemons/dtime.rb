#
# src/daemons/dtime.rb
#
class DTime < Daemon

  class Time_Frame < Curse_Frame 

    def initialize(parent)
      @parent = parent
      super()
      @x, @y = 69, 0
    end

    def content
      @parent.time.strftime("%H:%M:%S")
    end

  end

  attr_accessor :time

  def init
    super
    @time_frame = Time_Frame.new(self)
    @time = Time.now
  end

  def update
    super
    @time = Time.now
  end

end
