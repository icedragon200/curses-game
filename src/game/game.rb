#
# src/game/game.rb
#
class Game

  attr_accessor :map

  def initialize
  end

  def new_game
    create_game_objects
  end

  def create_game_objects
    @map = Game::Map.new
  end

  def load
  end

  def save
  end

end
