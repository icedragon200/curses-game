#
# src/linara/lin_main.rb
#
# Linara is a linear loading module, used to 
# execute certain blocks based on priority.
# 0 being the greatest priority
#
Linara.add_load("Main", 0) do
  Main.init
end

Linara.add_load("Graphics", 1) do 
  Graphics.init
end

Linara.add_load("Daemon", 2) do
  Daemon.init

  $dtime = DTime.new
  $dtask = DTask.new
  $dfps = DFPS.new

  Daemon.init_daemons
end  
