#
# src/mixin/curselem.rb
#

##
# class CurseElem
#
class Curse_Elem

  def initialize
    curse_init
    init
  end

  def dispose
    curse_dispose
  end

  def update
  end

private
  def init
  end

  def curse_init
  end

  def curse_dispose
  end

end
