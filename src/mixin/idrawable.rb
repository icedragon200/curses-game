#
# src/mixin/idrawable.rb
#

##
# interface IDrawable
#
module IDrawable

private
  def curse_init
    Graphics.add_drawable(self)
  end

  def curse_dispose
    Graphics.rem_drawable(self)
  end

public
  def draw
  end

  def can_draw?
    true
  end

end
