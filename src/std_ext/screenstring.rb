=begin
#
# ruby/games/Aseyran/std_ext/string.rb
#

#depreceated
class ScreenString
  
  ##
  # initialzie(int width, int height)
  #
  def initialize(width, height)
    @width, @height = width, height
    @data = Array.new(@height) do " " * @width end
  end

  ##
  # index2xy(int index)
  #
  def index2xy(index)
    return (index % @width), (index / @height)
  end

  ##
  # []=(int index)
  #
  def [](index)
    x, y = index2xy(index)
    return @data[y][x]
  end

  ##
  # []=(int index, String value)
  #
  def []=(index, char)
    raise("Character length should be 1") if char.length != 1
    x, y = index2xy(index)
    @data[y][x] = char
  end

  def write_to(x, y, string)
    string.split("").each_with_index do |s, i|
      self[x + i + (y * @width)] = s
    end
  end

  def read_from(x, y, length)
  end

  CHAR_NEWLINE = "\n"

  def draw!(io=$stdout)
    @data.each do |str|
      io.write(str + CHAR_NEWLINE) 
    end
  end

end
=end
