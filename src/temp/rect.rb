#
# src/temp/rect.rb
#
class Rect

  attr_accessor :x, :y, :width, :height
  
  def initialize(x, y, w, h)
    @x, @y, @width, @height = x, y, w, h
  end

  def to_s
    puts "#{self.class}: #{x}, #{y}, #{width}, #{height}"
  end

end
