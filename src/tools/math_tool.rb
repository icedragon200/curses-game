#
# src/tools/math_tool.rb
#
module MathTool

  def self.min(val, bot)
    val < bot ? val : bot
  end

  def self.max(val, top)
    val < top ? top : val
  end

  def self.clamp(val, nmin, nmax)
    val > nmin ? (val < nmax ? nmax : val) : nmin
  end

end
