#
# src/tools/string_tool.rb
#
module StringTool

  CHAR_NEWLINE = "\n"

  def self.pop(str)
    char = str[-1]
    str.replace(str[0...str.length-1])
    return char
  end

  def self.shift(str)
    char = str[0]
    str.replace(str[1...str.length])
    return char
  end

  def self.crop(str, rect)
    raise("Rectangle provided is invalid #{rect.to_s}") unless rect
    #puts rect

    lines = rect.height
    str_lines = grab_lines(str, rect.y...(rect.y + lines))
    str_size = rect.x...(rect.x + rect.width)
    str_lines.collect! do |s|
      new_s = s[str_size] || ""
      pop(new_s) if new_s.end_with?(CHAR_NEWLINE)
      new_s
    end
    return str_lines.join(CHAR_NEWLINE)
  end

  def self.grab_lines(str, range)
    str.each_line.to_a[range]
  end

  def self.get_width(str)
    #return 0 if str.length == 0
    lines = str.each_line
    longest_line = lines.max_by(&:length)
    return longest_line.length - 1
  end

  def self.get_height(str)
    str.count("\n") + 1
  end

end
