#
# src/visual/curse_frame.rb
#
class Curse_Frame < Curse_Elem

  include IDrawable

  attr_accessor :x, :y, :z

  def initialize()
    @x = @y = @z = 1
    super()
  end

  ##
  # content
  #
  # return String
  def content
    ""
  end
   
  def draw
    Graphics.addstr_at_content(self.x, self.y, content)#"░▒▓▄█▀")
  end

end
