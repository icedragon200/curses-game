#
# src/visual/curse_viewport.rb
#
class Curse_Viewport

  attr_accessor :rect, :z

  def initialize(x=0, y=0, w=Graphics.width, h=Graphics.height)
    @rect = Rect.new(x, y, w, h)
    @z = 0
  end

  def translate(x, y, z, str)
    nx = MathTool.max(rect.x - x, 0)
    ny = MathTool.max(rect.y - y, 0)
    wd = StringTool.get_width(str)
    hg = StringTool.get_height(str)
    crop_rect = Rect.new(nx, ny, wd - nx, hg - ny)
    return x + @rect.x, y + @rect.y, z + @z, StringTool.crop(str, crop_rect)
  end

end
