#
# src/visual/ecurse_gauge.rb
#
class ECurse_Gauge < Curse_Frame

  attr_reader :rate, :width, :height


  def initialize(width, height)
    super()
    @width = width
    @height = height
    @rate = 0.0
  end

  def set_rate(new_rate)
    @rate = new_rate
  end

  def rate=(new_rate)
    if @rate != new_rate
      set_rate(new_rate)
      refresh
    end
  end

  def refresh
    @str = nil
  end

  def content
    @str ||= begin
      ww = width - 2 - 4 
      str = " " * ww
      str[0] = str[-1] = "|"
      wr = ((ww) * rate).to_i

      (0...(ww)).each do |i| str[i] = "-" end
      (0...wr).each do |i| str[i] = "#" end
      
      str = "|" + str + "|" + "%-03s%" % (rate * 100).to_i

      if @height > 1
        str = ([str] * @height).join("\n")
      end
      str
    end  
    @str
  end

end
